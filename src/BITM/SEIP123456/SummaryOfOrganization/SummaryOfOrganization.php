<?php
namespace App\SummaryOfOrganization;
use App\Message\Message;
use App\Utility\Utility;
use App\Model\Database as DB;
use PDO;

class SummaryOfOrganization extends DB{
    public $id= "";
    public $organization= "";
    public $summary= "";


    public function __construct(){
        parent::__construct();
        if (!isset($_SESSION)) session_start();
    }

    public function setData($post = NULL){
        if (array_key_exists('id', $post)){
            $this->id= $post['id'];
        }
        if (array_key_exists('organization', $post)){
            $this->organization = $post['organization'];
        }
        if (array_key_exists('summary', $post)){
            $this->summary = $post['summary'];
        }

    }
    public function store(){
        $arrData = array( $this->organization, $this-> summary);

        $sql = "Insert INTO summary_of_organization(organization, summary) VALUES (?,?)";
        $STH = $this->DBH->prepare($sql);

        $result = $STH->execute($arrData);

        if($result)
            Message::message("Success! Data Has Been Inserted Successfully :)");
        else
            Message::message("Failed! Data Has Not Been Inserted Successfully :(");


        Utility::redirect('create.php');

    }


    public function index($fetchMode = 'ASSOC')
    {

        $STH = $this->DBH->query('SELECT * from summary_of_organization where is_deleted = \'No\'');

        $fetchMode = strtoupper($fetchMode);
        if (substr_count($fetchMode, 'OBJ') > 0)
            $STH->setFetchMode(PDO::FETCH_OBJ);
        else
            $STH->setFetchMode(PDO::FETCH_ASSOC);

        $arrAllData = $STH->fetchAll();
        return $arrAllData;

    }
// end of index();
    public function view($fetchMode = 'ASSOC'){
        $sql= 'SELECT * from summary_of_organization WHERE id =' .$this->id;


        $STH = $this->DBH->query($sql);

        $fetchMode = strtoupper($fetchMode);
        if (substr_count($fetchMode, 'OBJ') > 0)
            $STH->setFetchMode(PDO::FETCH_OBJ);
        else
            $STH->setFetchMode(PDO::FETCH_ASSOC);

        $arroneData = $STH->fetch();
        return $arroneData;

    }
// end of view();

    public function update(){
        // UPDATE `atomic_project_b35`.`book_title` SET `book_title` = 'nisi umm', `author_name` = 'nisi rtg t' WHERE `book_title`.`id` = 4;
        $arrData= array($this->organization, $this->summary);
        $sql= "Update summary_of_organization SET organization = ?, summary = ?  WHERE id =". $this->id;
        $STH= $this->DBH->prepare($sql);
        $STH-> execute($arrData);
        Utility::redirect('index.php');


    }
    // end of update();
    public function delete(){
        $sql= "DELETE FROM summary_of_organization WHERE id =". $this->id;
        //$sql= "DELETE FROM book_title WHERE id =". $this->id;
        $STH= $this->DBH->prepare($sql);
        $STH-> execute();
        Utility::redirect('index.php');


    }
    // end of delete



    public function trash(){

        $sql = "Update summary_of_organization SET is_deleted=NOW() where id=".$this->id;

        $STH = $this->DBH->prepare($sql);

        $STH->execute();

        Utility::redirect('index.php');


    }// end of trash()



    public function trashed($fetchMode='ASSOC'){
        $sql = "SELECT * from summary_of_organization where is_deleted <> 'No' ";
        $STH = $this->DBH->query($sql);

        $fetchMode = strtoupper($fetchMode);
        if(substr_count($fetchMode,'OBJ') > 0)
            $STH->setFetchMode(PDO::FETCH_OBJ);
        else
            $STH->setFetchMode(PDO::FETCH_ASSOC);

        $arrAllData  = $STH->fetchAll();
        return $arrAllData;


    }// end of trashed();





    public function recover(){

        $sql = "Update summary_of_organization SET is_deleted='No' where id=".$this->id;

        $STH = $this->DBH->prepare($sql);

        $STH->execute();

        Utility::redirect('index.php');

    }// end of recover();



}

// end of city Class

?>



